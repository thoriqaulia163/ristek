from django.shortcuts import render,redirect
from .models import Post, Komentar
from .forms import FormKomentar
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, auth
from django.http import JsonResponse
from django.core import serializers

# Create your views here.
# @login_required(login_url='/authentication/login')

@login_required(login_url='/authentication/login')
def mypost(request):
    allpost = Post.objects.filter(nama = request.user.username)
    responses = {
        'allpost' : allpost,
        'postcount' : allpost.count()
    }
    return render(request, 'blogpost/mypost.html', responses)

@login_required(login_url='/authentication/login')
def createpost(request):
    if request.method == "POST":
        title = request.POST['title']
        textpost = request.POST['textpost']
        name = request.user.username
        Post.objects.create(judul=title, post=textpost, nama=name)
        return redirect('/blogpost/mypost')
    return render(request, 'blogpost/createpost.html')

@login_required(login_url='/authentication/login')
def updatepost(request,id):
    post = Post.objects.get(id=id)
    if request.method == "POST":
        title = request.POST['title']
        textpost = request.POST['textpost']
        name = request.user.username
        post.judul = title
        post.post = textpost
        post.save()
        return redirect('/blogpost/mypost')
    response = {
        'title' : post.judul,
        'textpost' : post.post
    }
    return render(request, 'blogpost/updatepost.html',response)

@login_required(login_url='/authentication/login')
def deletepost(request, id):
    post = Post.objects.get(id=id) 
    post.delete()
    return redirect('/blogpost/mypost')

def blogpost(request):
    responses = {
        'allpost' : Post.objects.filter(),
        'postcount' : Post.objects.count()
    }
    return render(request, 'blogpost/blogpost.html', responses)

def searchpost(request):
    keyword = request.GET.get('q',"")

    datatemp = []
    if keyword != "":
        for isi in Post.objects.all():
            if keyword in isi.judul: 
                item = {'judul' : isi.judul,'nama': isi.nama, 'waktu': isi.waktu, 'post':isi.post, 'key': isi.id}
                datatemp.append(item)
    else:
        for isi in Post.objects.all():
            item = {'judul' : isi.judul,'nama': isi.nama, 'waktu': isi.waktu, 'post':isi.post, 'key': isi.id}
            datatemp.append(item)
    return JsonResponse({'data':datatemp})

def postdetail(request,id):
    post = Post.objects.get(id=id)
    komentar = Komentar.objects.filter(post__id = post.id)

    if request.user.is_active:
        form = FormKomentar(initial= {'post': id, 'nama': request.user.username})
    else:
        form = FormKomentar(initial= {'post': id})
    responses = {
        'komentar': komentar,
        'post': post,
        'form' : form,
    }
    return render(request, 'blogpost/postdetail.html', responses)

def addcomment(request,id,name):
    post = Post.objects.get(id=id)
    datatemp = []
    if request.POST:
        komentar = request.POST.get('komentar')
        post_id = request.POST.get('post')
        nama = request.POST.get('nama')
        Komentar.objects.create(
            komentar = komentar,
            post = Post.objects.get(id=post_id),
            nama = nama,
            )
        for isi in Komentar.objects.filter(post__id = post.id):
            item = {'nama':isi.nama, 'komentar':isi.komentar, 'waktu': isi.waktu}
            datatemp.append(item)
        return JsonResponse({'data':datatemp})
