from django.urls import path
from . import views

urlpatterns = [
    path('', views.blogpost, name='blogpost'),
    path('searchpost', views.searchpost, name='searchpost'),
    path('mypost', views.mypost, name='mypost'),
    path('createpost', views.createpost, name='createpost'),
    path('updatepost/<int:id>', views.updatepost, name='updatepost'),
    path('deletepost/<int:id>', views.deletepost, name='deletpost'),
    path('postdetail/<int:id>', views.postdetail, name='postdetail'),
    path('addcomment/<int:id>/<str:name>', views.addcomment, name='addcomment'),
]