from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import auth, User
from django.views.decorators.csrf import csrf_exempt


def login_view(request):
    response = {'pesan': ''}
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(request, username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/blogpost')
        response = {'pesan': 'Login gagal'}
    return render(request, 'authentication/login.html',response)

def logout_view(request):
    logout(request)
    return redirect('/')

def register_view(request):
    response = {'pesan': ''}
    if request.method == "POST":
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        confirm_password = request.POST['confirmPass']

        all_user = User.objects.all()
        for user in all_user:
            if username == user.username:
                return render(request,'authentication/register.html', {'pesan':'username has used'})

        if password == confirm_password:
            user = User.objects.create_user(username=username, email=email, password = password)
            user.save()

            user = auth.authenticate(username=username, password=password)
            auth.login(request, user)
            return redirect('/blogpost')

        else:
            response = {'pesan': 'password confirm unmatch'}
    return render(request, 'authentication/register.html', response)