$("#pencarian").keyup(function () {
    var keyword = $("#pencarian").val();
    $.ajax({
        url: 'searchpost?q=' + keyword,
        success: function (data) {
            var array_items = data.data;
            $(".post-list-container").empty();
            for (i = 0; i < array_items.length; i++) {
                var judul = array_items[i].judul;
                var post = array_items[i].post.slice(0,99);
                var nama = array_items[i].nama;
                var waktu = array_items[i].waktu;
                var id = array_items[i].key;
                console.log(judul);
                $(".post-list-container").append("<div class='post-container'><a href='/blogpost/postdetail/"+ id +"' ><div class='post-title'>" + judul + "</div></a><div class='post-text'><p>"+ post +"...</p></div><div class='post-info'><div class='post-creator'>"+ nama +"</div><div class='post-time'>"+ waktu +"</div></div></div>");
            }
            $(".input-box").val("");
        }
    });

});