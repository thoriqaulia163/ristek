$('form').on('submit',function(e){
    e.preventDefault();
        $.ajax({
        type:$(this).attr('method'),
        url: '/blogpost/addcomment/'+$(this).attr('postID')+'/'+$('input[name=nama]').val(),
        data:{
            komentar:$('input[name=komentar]').val(),
            post:$('input[name=post]').val(),
            nama:$('input[name=nama]').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
        },
        success: function(data){
            var array_items = data.data;
            $(".comment-container").empty();
            for(i = 0; i < array_items.length; i++){
                 var header =  '<div class="comment-box">';
                 var nama = '<h2>'+array_items[i].nama+'</h2>'
                 var komentar = '<p>'+array_items[i].komentar+'</p>'
                 var waktu = '<p class="comment-waktu">'+array_items[i].waktu+'</p>'
                 $(".comment-container").append(header+nama+komentar+waktu+"</div>");
                }
            $("input[name=komentar]").val("")
            }
        });
});