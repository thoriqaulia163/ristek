from django.contrib import admin
from .models import Komentar, Post


admin.site.register(Post)
admin.site.register(Komentar)
