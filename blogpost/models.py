from django.db import models
from datetime import datetime, date

class Post(models.Model):
    judul = models.CharField(max_length=60)
    post = models.TextField()
    nama = models.CharField(max_length=50)
    waktu = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.judul

class Komentar(models.Model):
    nama = models.CharField(max_length=10)
    komentar = models.TextField(max_length=100)
    waktu = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True)
    
    def __str__(self):
        return self.nama